# syntax=docker/dockerfile:1

# building
FROM golang:alpine AS build
LABEL autodelete=true
WORKDIR /app
COPY . .
RUN go mod download
RUN go build -o app main.go

# application
FROM alpine:latest
COPY --from=build /app/app /app
CMD [ "/app"]