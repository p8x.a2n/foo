package main

import (
	"fmt"
	"hash/crc32"
)

func main() {
	f0()
}

func f0() {
	fmt.Printf("%x\n", crc32.ChecksumIEEE([]byte("0")))
}
