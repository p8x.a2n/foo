package main

import (
	"fmt"
	"sort"
	"sync"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func main() {
	var dsn = "root:secret@tcp(localhost:3306)/test?charset=utf8&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), nil)
	if err != nil {
		panic(err)
	}

	if err := db.AutoMigrate(&testModel{}); err != nil {
		panic(err)
	}

	if err := purge(db); err != nil {
		panic(err)
	}
	if err := batchCreate(db); err != nil {
		panic(err)
	}
	if err := query(db); err != nil {
		panic(err)
	}

	if len(controlGroup) != len(testGroup) {
		panic("mismatch size")
	}
	for i := range controlGroup {
		if controlGroup[i].ID != testGroup[i].ID {
			fmt.Printf("mismatch id, %d != %d\n", controlGroup[i].ID, testGroup[i].ID)
			continue
		}
		if controlGroup[i].Value != testGroup[i].Value {
			fmt.Printf("mismatch value, %d != %d\n", controlGroup[i].Value, testGroup[i].Value)
			continue
		}
	}
}

type testModel struct {
	gorm.Model
	Value int
}

func purge(db *gorm.DB) error {
	if err := db.Exec("DELETE FROM test_models").Error; err != nil {
		return err
	}
	return nil
}

var testGroup []*testModel

func batchCreate(db *gorm.DB) error {
	var (
		rwMux sync.RWMutex
		wg    sync.WaitGroup
	)
	for i := 0; i < 1e2; i++ {
		wg.Add(1)
		go func(val int) {
			var data = &testModel{Value: val}
			if err := db.Create(&data).Error; err != nil {
				fmt.Println(err.Error())
				return
			}
			rwMux.Lock()
			testGroup = append(testGroup, data)
			rwMux.Unlock()
			wg.Done()
		}(i)
	}
	wg.Wait()

	sort.SliceStable(testGroup, func(i, j int) bool {
		return testGroup[i].ID < testGroup[j].ID
	})

	return nil
}

var controlGroup []*testModel

func query(db *gorm.DB) error {
	if err := db.Find(&controlGroup).Error; err != nil {
		return err
	}
	sort.SliceStable(controlGroup, func(i, j int) bool {
		return controlGroup[i].ID < controlGroup[j].ID
	})
	return nil
}
